/**********************************
 * Import modules
 **********************************/
const path = require('path');
const fs = require('fs');
const Promise = require('bluebird');
const express = require('express');
const router = express.Router();
const oracledb = require('oracledb');
oracledb.maxRows = 1000;
oracledb.outFormat = oracledb.OBJECT;

const {editImage, preGenerate} = require('../libs/imageEditor');

const batchLimit = 100;

/**********************************
 * Main
 **********************************/
router.post('/resolution', (req, res, next) => {
  try {
    fs.writeFileSync(path.join(__dirname, '../data/resolution.json'), JSON.stringify(req.body.resolutions));
    res.send(200).status('Updated');
  } catch (error) {
    res.render(path.join(__dirname, '../views/error'), {error, message: error.message});
  }
});

router.get('/admin', async (req, res, next) => {
  try {
    let resolutions = fs.readFileSync(path.join(__dirname, '../data/resolution.json'), {encoding: 'UTF-8'});

    res.render(path.join(__dirname, '../views/index'), {
      resolutions: JSON.parse(resolutions)
    });
  } catch (error) {
    res.render(path.join(__dirname, '../views/error'), {error, message: error.message});
  }
});

router.get('/pregen', async (req, res, next) => {
  console.log('pre-gereating');
  preGenerate(req.query);
  res.status(200).send('Done.');
});

router.get('/', async (req, res, next) => {
  try {
    let result = await editImage(req.query);

    if (result.contentType) {
      res.setHeader('Content-Type', result.contentType);
    } else {
      res.setHeader('Content-Type', 'image/jpeg');
    }

    res.end(result.data);
  } catch (error) {
    console.log('routing catch error', error);
    res.status(200).send();
  }
});

/**********************************
 * Export
 **********************************/
module.exports = router;