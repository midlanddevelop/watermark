/***********************************************
 * Params: src, wm, q, w, h, fcrop, bg, hvm
 * Mandatory: src, wm
 ***********************************************/

/**********************************
 * Import modules
 **********************************/
const path = require('path');
const Jimp = require('jimp');
const Promise = require('bluebird');
const Color = require('color');
const mkdirp = require('mkdirp');
const fs = require('fs');
const oracledb = require('oracledb');
const AWS = require('aws-sdk');
const now = require('performance-now');

oracledb.maxRows = 1000;
oracledb.outFormat = oracledb.OBJECT;

/**********************************
 * Const
 **********************************/
AWS.config.update({
  accessKeyId: 'AKIAIRYUD6WLQ7NHN4EA',
  secretAccessKey: '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',
});
const s3 = new AWS.S3();
const batchLimit = 100;
const AWS_BUCKET = 'res.midland.com.hk';

const useS3 = true;

const destPath = path.join(__dirname, '../public');
const prefix = 'http://img.midland.com.hk/watermark_logo';
const logoUrls = {
  'mh': `${prefix}/midland_holding.png`,
  'mr': `${prefix}/mr_logo2.png`,
  'mo': `${prefix}/macau_logo.png`,
  'hkp': `${prefix}/hkp_logo2.png`,
  'ici': `${prefix}/midlandici_logo_459.png`,
  'i': `${prefix}/midlandici_logo_indus.png`,
  'c': `${prefix}/midlandici_logo_comm.png`,
  's': `${prefix}/midlandici_logo_shop.png`,
  'icb': `${prefix}/hkpici_logo2.png`,
  'hkpi': `${prefix}/hkpici_logo2_indus.png`,
  'hkpc': `${prefix}/hkpici_logo2_comm.png`,
  'hkps': `${prefix}/hkpici_logo2_shop.png`,
  'hkpmo': `${prefix}/hkp_mo_logo.png`
};

let totalCacheTimes = 0, totalUploadTimes = 0, totalProcessTimes = 0, totalGetImagesTimes = 0;
let overallStartTime;

/**********************************
 * Main
 **********************************/
const editImage = (query = {}) => {
  return new Promise(async (resolve, reject) => {
    query.w = !isNaN(Number(query.w)) ? Number(query.w) : 0;
    query.h = !isNaN(Number(query.h)) ? Number(query.h) : 0;

    try {
      query = await checkMandatory(query);

      let getCache_start = now();
      let cachedImage = await getCache(query);
      totalCacheTimes += Number((now()-getCache_start).toFixed(3));

      if (cachedImage) {
        resolve(cachedImage);
      } else {
        let start = now();
        const {source, logo} = await getImageAndLogo(query);
        totalGetImagesTimes += Number((now() - start).toFixed(3));

        let image = await processImage(source, logo, query);
        resolve(image);
      }
    } catch (error) {
      reject(error);
    }
  });
};

const preGenerate = (query = {}) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (query.wm) {
        let logoImg = await Jimp.read(logoUrls[query.wm.trim().toLowerCase()]);
        let resolutions = fs.readFileSync(path.join(__dirname, '../data/resolution.json'), {encoding: 'UTF-8'});
        let connection = await getDBConnection();
  
        // get count records from database
        let total_result = await connection.execute(`
          select count(0) as count from mrms_stock_doc where doc_code = 'PHOTO'
        `);
        releaseConnection(connection);

        // start pre-gerenate
        scanDBAndGenerate({
          ...query, 
          total: total_result.rows[0].COUNT,
          current: 0,
          logoImg,
          resolutions
        });
      } else {
        reject({code: 400, message: `Missing wm`});
      }
    } catch (error) {
      reject(error);
    }
  });
};

/**********************************
 * Functions
 **********************************/
const checkMandatory = (query = {}) => {
  return new Promise((resolve, reject) => {
    if (!query.src || (!query.wm && !query.logoImg) || (query.wm && !logoUrls[query.wm.trim().toLowerCase()])) {
      reject('Missing mandatory fields');
    } else {
      const {src} = query;
      let parts = src.split('/');
      let filenameParts = parts.length > 0 ? parts[parts.length - 1].split('.') : 0;
      query.filename = filenameParts[0];
      query.extension = filenameParts[1];
      query.folderName = query.folderName || query.filename;
      resolve(query);
    }
  });
};

const getDBConnection = () => {
  return oracledb.getConnection({
    user: 'digital',
    password: 'digital123',
    connectString: 'db1.midland.com.hk/db1'
  });
};

const getCache = async (query = {}) => {
  let __path = path.join(getDestPath(query), getDestFilename(query));

  if (fs.existsSync(path.join(destPath, __path))) {
    return {data: fs.readFileSync(path.join(destPath, __path))};
  } else {
    if (useS3) {
      try {
        let img = await getImgFromS3({Bucket: AWS_BUCKET, Key: __path});
        return {data: img};
      } catch (error) {
        return false;
      }
    } else {
      return false;
    }
  }
};

const getDestPath = (query = {}) => {
  return path.join('images/wm', query.wm, 'stockphoto', query.folderName.substr(query.folderName.length - 3), `${query.w}x${query.h}`);
};

const getDestFilename = (query = {}) => {
  let name = query.filename;

  if (query.wm && query.wm.trim() !== '') {
    name += '_' + query.wm.trim();
  }

  // if (query.w && query.w.trim() !== '') {
  //   name += '_w' + query.w.trim();
  // }

  // if (query.h && query.h.trim() !== '') {
  //   name += '_h' + query.h.trim();
  // }

  if (query.q && query.q.trim() !== '') {
    name += '_q' + query.q.trim();
  }

  if (query.fcrop && query.fcrop.trim() !== '') {
    name += '_fcrop#' + query.fcrop.trim();
  }

  if (query.hvm && query.hvm.trim() !== '') {
    name += '_hvm#' + query.hvm.trim();
  }

  if (query.bg && query.bg.trim() !== '') {
    name += '_bg#' + query.bg.trim();
  }

  name += '.' + query.extension;

  return name;
};

const getImageAndLogo = (query = {}) => {
  return new Promise(async (resolve, reject) => {
    try {
      let promises = [];
  
      if (query.src.includes('.s3.amazonaws.com')) {
        promises.push(getImgFromS3({...query, jimpImage: true}));
      } else {
        promises.push(Jimp.read(query.src));
      }
  
      if (!query.logoImg) {
        promises.push(Jimp.read(logoUrls[query.wm.trim().toLowerCase()]));
      }
  
      let results = await Promise.all(promises);
      
      let data = {
        source: (results[0] instanceof Jimp) ? results[0] : await Jimp.read(results[0]),
        logo: query.logoImg ? query.logoImg : results[1]
      };
  
      if (data.source && data.logo) {
        resolve(data);
      } else {
        reject({code: 400, message: 'Missing source or logo'});
      }
    } catch (error) {
      reject(error);
    }
  });
};

const getImgFromS3 = (options = {}) => {
  return new Promise((resolve, reject) => {
    let Bucket = options.Bucket,
        Key = options.Key;

    // console.log('Bucket', Bucket + '/' + Key);
    if (options.src && (!Bucket || !Key)) {
      let parts = options.src.replace('http://', '').replace('https://', '').split('/');
      Bucket = Bucket || parts.shift().replace('.s3.amazonaws.com', '');
      Key = Key || parts.join('/');
    }

    if (Bucket && Key) {
      s3.getObject({ Bucket, Key }, async (err, data) => {
        if (err) {
          reject(err);
        } else {
          if (options.jimpImage) {
            resolve(Jimp.read(data.Body));
          } else {
            resolve(data.Body);
          }
        }
      });
    } else {
      reject({code: 400, message: 'Missing Bucket or Key'});
    }
  })
};

const processImage = (source, logo, query) => {
  let start = now();
  return new Promise((resolve, reject) => {
    logo = logo.clone();

    // crop image
    if ((query.fcrop == 'Y' || query.crop == 'Y') && query.w > 0 && query.h > 0) {
      source.cover(query.w, query.h);
    } else {
      // resize src image
      source = resizeImage(source, query.w, query.h);
    }

    // create background image
    if ((query.bg && query.bg !== '' && query.w > 0 && query.h > 0) || (source.bitmap.height < query.h || source.bitmap.width < query.w)) {
      let bg = query.bg || 'ffffff';
      let background = new Jimp(query.w, query.h, Jimp.rgbaToInt(...Color(`#${bg}`).array(), 255));
      source = background.composite(source, (background.bitmap.width - source.bitmap.width)/2, (background.bitmap.height - source.bitmap.height)/2);
    }
  
    // set quality
    if (query.q) {
      source.quality(Number(query.q));
    }
  
    // resize logo
    logo.opacity(0.3);
    logo = resizeImage(logo, Math.round(source.bitmap.width / 4), Math.round(source.bitmap.height / 4));
  
    if (logo.bitmap.width >= 400) {
      logo = resizeImage(logo, Math.round(source.bitmap.width / 3), Math.round(logo.bitmap.height));
    }
  
    // add watermark
    if (query.hvm !== 'Y') {
      source = addWatermarks(source, logo);
    }
  
    totalProcessTimes += Number((now() - start).toFixed());
    source.getBuffer(source.getMIME(), (err, _buffer) => {
      if (err) {
        reject(err);
      } else {
        saveImage(query, source, _buffer);
        resolve({contentType: source.getMIME(), data: _buffer});
      }
    });
  });
};

const releaseConnection = (connection) => {
  connection.release(err => {
    if (err) {
      console.log('release connection error', err);
      connection.break(err => {
        console.log('break connection error', err);
      });
    }
  });
};

const scanDBAndGenerate = (options = {}) => {
  return new Promise(async (resolve, reject) => {
    let perc = 0,
        failedCase = [],
        start = now();

    options.total -= batchLimit;
    let connection = await getDBConnection();

    connection.execute(`
      select * from (
        select rownum rn, msd.wan_doc_path, msd.stock_id from mrms_stock_doc msd where msd.doc_code = 'PHOTO' order by msd.serial_no, msd.update_date
      ) where rn > ${options.current} and rn <= ${options.current + batchLimit}
      order by rn
    `).then(({rows}) => {
      releaseConnection(connection);
      overallStartTime = new Date();
      return Promise.map(rows, async (row, index) => {
        return Promise.map(JSON.parse(options.resolutions), async data => {
          let resolution = data.split('x');
          let conditions = {
            ...options, 
            src: row.WAN_DOC_PATH, 
            w: resolution[0], 
            h: resolution[1],
            folderName: row.STOCK_ID.trim()
          };

          let progressLog = () => console.log(`Batch ${options.current + 1}-${options.current + batchLimit} records: ${(++perc/batchLimit*100).toFixed(1)}%`);

          return editImage(conditions)
            .then(result => {
              progressLog();
            })
            .catch(err => {
              progressLog();
              failedCase.push(conditions);
            })
        });
      }).then(async _results => {
        if (failedCase.length > 0) {
          console.log(`retrying ${failedCase.length} records...`);
          await Promise.map(failedCase, (_record) => {
            return editImage(_record);
          });
        }
        console.log('next batch...');
        // console.log('Average get cache time', totalCacheTimes / batchLimit);
        // console.log('Average totalGetImagesTimes time', totalGetImagesTimes / batchLimit);
        // console.log('Average process time', totalProcessTimes / batchLimit);
        // console.log('Average upload time', totalUploadTimes / batchLimit);
        // totalCacheTimes = 0;
        // totalGetImagesTimes = 0;
        // totalProcessTimes = 0;
        // totalUploadTimes = 0;

        options.current += batchLimit;

        if (options.current <= options.total) {
          console.log('Whole progress time', (now() - start).toFixed(3));
          // scanDBAndGenerate(options);
        }
      }).catch(err => {
        console.log('error', err);
      });
    });
  });
};

/**********************************
 * Generate Image Functions
 **********************************/
const addWatermarks = (source, logo) => {
  return source
          .composite(logo, (source.bitmap.width / 4) - (logo.bitmap.width / 2), (source.bitmap.height / 4) - (logo.bitmap.height / 2)) // top-left
          .composite(logo, (source.bitmap.width / 4 * 3) - (logo.bitmap.width / 2), (source.bitmap.height / 4) - (logo.bitmap.height / 2)) // top-right
          .composite(logo, (source.bitmap.width / 4) - (logo.bitmap.width / 2), (source.bitmap.height / 4 * 3) - (logo.bitmap.height / 2)) // bottom-left
          .composite(logo, (source.bitmap.width / 4 * 3) - (logo.bitmap.width / 2), (source.bitmap.height / 4 * 3) - (logo.bitmap.height / 2)); // bottom-right
};

const resizeImage = (img, targetWidth, targetHeight) => {
  let finalWidth, finalHeight;
  targetHeight = (!targetHeight || targetHeight == 0) ? img.bitmap.height : targetHeight;
  targetWidth = (!targetWidth || targetWidth == 0) ? img.bitmap.width : targetWidth;

  if (img.bitmap.width / img.bitmap.height >= targetWidth / targetHeight) {
    finalWidth = (img.bitmap.width >= targetWidth) ? targetWidth : (targetHeight * img.bitmap.width) / img.bitmap.height;
    finalHeight = (finalWidth * img.bitmap.height) / img.bitmap.width;
  } else {
    finalHeight = (img.bitmap.height >= targetHeight) ? targetHeight : (targetWidth * img.bitmap.height) / img.bitmap.width;
    finalWidth = (finalHeight * img.bitmap.width) / img.bitmap.height;
  }

  return img.resize(finalWidth, finalHeight);
};

const saveImage = (query, source, _buffer) => {
  // path: images/wm/mr/stockphoto/{last_three_digital}/{size}/{file name with parameter}
  let __path = path.join(getDestPath(query), getDestFilename(query));

  if (useS3) {
    let start = now();
    s3.putObject({
      Bucket: AWS_BUCKET,
      Key: __path,
      Body: _buffer,
      ACL: 'public-read'
    }, (err, data) => {
      if (err) {
        console.log('s3 putObject err', err);
      } else {
        totalUploadTimes += Number((now()-start).toFixed(3));
        // console.log(`uploaded to s3(${AWS_BUCKET})`, __path, (now()-start).toFixed(3), totalUploadTimes/batchLimit);
        console.log('uploaded', overallStartTime, new Date())
      }
    });
  } else {
    mkdirp(destPath, (err, made) => {
      if (err) {
        console.log('mkdirp error', err);
      } else {
        source.write(path.join(destPath, __path));
      }
    });
  }
};

/**********************************
 * Export
 **********************************/
module.exports = {
  editImage,
  preGenerate
};